Created a bitbucket repository.

Cloned repo into Cloud9

Start CouchDB according to - https://community.c9.io/t/setting-up-couchdb/1715

Verify that CouchDB is started by doing curl http://127.0.0.1:5984

Expose CouchDB externally with script c9couch.js from https://github.com/romannep/c9couch

Used http://www.convertcsv.com/csv-to-json.htm for simple CSV to JSON conversion
# http://www.csvjson.com/csv2json 

Tips on importing http://stackoverflow.com/questions/790757/import-json-file-to-couch-db

Import the data with:
#curl -vX POST http://127.0.0.1:5984/opendata -d @- -# -o output -H "Content-Type: application/json" < kommunalaSkattesatser.json
curl -d @kommunalaSkattesatser.json -H "Content-type: application/json" -X POST http://127.0.0.1:5984/kommunalaskattesatser/_bulk_docs
